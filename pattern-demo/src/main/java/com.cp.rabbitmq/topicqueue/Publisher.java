package com.cp.rabbitmq.topicqueue;


import java.io.IOException;
import java.util.concurrent.TimeoutException;


import com.cp.rabbitmq.utils.ConnectionUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
/**
 * @author Coder编程
 * @version V1.0
 * @Title: Publisher
 * @Package: com.cp.rabbitmq.topicqueue
 * @Description: TODO
 * @date 2019/8/7  19:44
 **/

public class Publisher {

    private static final String EXCHANGE_NAME = "TEST_EXCHANGE_TOPIC";

    public static void main(String[] args) throws IOException, TimeoutException {


        Connection connection = ConnectionUtils.getConnection();

        Channel channel = connection.createChannel();

        //exchange
        channel.exchangeDeclare(EXCHANGE_NAME, "topic");

        String msgString="商品....";
        channel.basicPublish(EXCHANGE_NAME, "goods.delete", null, msgString.getBytes());
        System.out.println("[WQ ]send "+msgString);

        channel.close();
        connection.close();
    }
}
