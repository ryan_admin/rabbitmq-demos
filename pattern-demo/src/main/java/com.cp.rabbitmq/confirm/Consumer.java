package com.cp.rabbitmq.confirm;



import java.io.IOException;
import java.util.concurrent.TimeoutException;


import com.cp.rabbitmq.utils.ConnectionUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.AMQP.BasicProperties;
/**
 * @author Coder编程
 * @version V1.0
 * @Title: Consumer
 * @Package: com.cp.rabbitmq.confirm
 * @Description: TODO
 * @date 2019/8/7  20:05
 **/

public class Consumer {


    private static final String QUEUE_NAME="TEST_QUEUE_CONFIRM3";


    public static void main(String[] args) throws IOException, TimeoutException {

        Connection connection = ConnectionUtils.getConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);
        channel.basicConsume(QUEUE_NAME, true,new DefaultConsumer(channel){

            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       BasicProperties properties, byte[] body) throws IOException {
                System.out.println("Consumer[confirm] msg:"+new String(body,"utf-8"));
            }
        });
    }
}
