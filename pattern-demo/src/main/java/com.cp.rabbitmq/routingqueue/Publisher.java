package com.cp.rabbitmq.routingqueue;

import com.cp.rabbitmq.utils.ConnectionUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author Coder编程
 * @version V1.0
 * @Title: Publisher
 * @Package: com.cp.rabbitmq.routingqueue
 * @Description: 路由模式
 * @date 2019/7/10  21:16
 **/

public class Publisher {

    private static final String EXCHANGE_NAME = "TEST_EXCHANGE_DIRECT";

    public static void main(String[] args) throws IOException, TimeoutException {


        Connection connection = ConnectionUtils.getConnection();

        Channel channel = connection.createChannel();

        //exchange
        channel.exchangeDeclare(EXCHANGE_NAME, "direct");

        String msg = "hello direct!";


        String routingKey = "info";

        channel.basicPublish(EXCHANGE_NAME, routingKey, null, msg.getBytes());

        System.out.println("send " + msg);

        channel.close();
        connection.close();
    }
}
