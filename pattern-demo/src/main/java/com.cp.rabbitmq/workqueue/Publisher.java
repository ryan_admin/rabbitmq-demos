package com.cp.rabbitmq.workqueue;


/* /**
   * @author Coder编程
   * @Title: Publisher
   * @Package:  com.cp.rabbitmq.simplequeue
   * @Description: 
   * @version V1.0
   * @date 2019/7/9 21:41
 **/

import com.cp.rabbitmq.utils.ConnectionUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/* /**
   * @author Coder编程
   * @Title: Publisher
   * @Package:  com.cp.rabbitmq.workqueue
   * @Description: 发送消息
   * @version V1.0
   * @date 2019/7/9 22:10
 **/

public class Publisher {
    /*					|---C1
     *   P-----Queue----|
     * 	`				|---C2
     *
     */

    private static final String  QUEUE_NAME="TEST_WORK_QUEUE";

    public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
        //获取连接
        Connection connection = ConnectionUtils.getConnection();

        //获取channel
        Channel channel = connection.createChannel();

        //声明队列
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        for (int i = 0; i <50; i++) {

            String msg="hello "+i;
            System.out.println("[WQ ]send:"+msg);
            channel.basicPublish("", QUEUE_NAME, null, msg.getBytes());

            Thread.sleep(i*20);
        }

        channel.close();
        connection.close();
    }

}
