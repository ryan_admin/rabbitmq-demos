package com.cp.rabbitmq.publishersubscribe;

import com.cp.rabbitmq.utils.ConnectionUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.concurrent.TimeoutException;


/* /**
   * @author Coder编程
   * @Title: 发布订阅模式
   * @Package:  com.cp.rabbitmq.publishersubscribe
   * @Description: 
   * @version V1.0
   * @date 2019/7/10 19:52
 **/

public class Publisher {

	private static final String  EXCHANGE_NAME = "TEST_EXCHANGE_FANOUT";

	public static void main(String[] args) throws IOException, TimeoutException {
		
		Connection connection = ConnectionUtils.getConnection();
		
		Channel channel = connection.createChannel();
		
		//声明交换机
		channel.exchangeDeclare(EXCHANGE_NAME, "fanout");//分发
		
		//发送消息
		String msg="hello ps";
		
		channel.basicPublish(EXCHANGE_NAME, "", null, msg.getBytes());
		
		System.out.println("Send :"+msg);
		
		channel.close();
		connection.close();
	}
}